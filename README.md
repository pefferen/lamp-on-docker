# LAMP on Docker

Simple LAMP Stack in Docker. Use this repo as a base for your project, after cloning this repo remove `.git/`.

Stack consists of:

* Debian 9 Stretch
* Apache 2.4
* PHP 7.1
* MariaDB
* Adminer 4

## Docker setup

The stack is configured via Docker-compose. Make sure you have a recent version of Docker installed. Than setup via the following steps:

1. run `docker-compose up`
2. when the containers are setup visit you container via the browser http:/localhost/

## webserver

Put your web app files in `www` and access your site via http://localhost/

## Adminder

Access Adminer via http://localhost:8080/